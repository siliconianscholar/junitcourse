package com.in28minutes.business;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ListTest {

    @Test
    public void testMockListSize() {
        List listMock = mock(List.class);
        when(listMock.size()).thenReturn(2);

        assertEquals(2, listMock.size());
        assertEquals(2, listMock.size());
        assertEquals(2, listMock.size());
    }

    @Test
    public void testMockListSize_ReturnMultipleValues() {
        List listMock = mock(List.class);
        when(listMock.size()).thenReturn(2).thenReturn(3);

        assertEquals(2, listMock.size());
        assertEquals(3, listMock.size());
    }

    @Test
    public void testMockListGet() {
        List listMock = mock(List.class);
        when(listMock.get(0)).thenReturn("SiliconianScholar");

        assertEquals("SiliconianScholar", listMock.get(0));
        assertEquals(null, listMock.get(1)); // nice mock behaviour, returns defaults if not told how to what is the expected behaviour when a method is called, using EasyMock in place of Mockito results in test failures
    }

    @Test
    public void testMockListGet_WithArgMatchers() {
        List listMock = mock(List.class);
        //Argument Matchers
        when(listMock.get(anyInt())).thenReturn("SiliconianScholar");

        assertEquals("SiliconianScholar", listMock.get(0));
        assertEquals("SiliconianScholar", listMock.get(1));
    }

    @Test(expected = RuntimeException.class)
    public void testMockList_throwAnException() {
        List listMock = mock(List.class);
        //Argument Matchers, Mockito does not allow a combination of Matchers and a hardcoded value, e.g. can be tested with sublist method of List
        when(listMock.get(anyInt())).thenThrow(new RuntimeException("Testing Exception"));

        listMock.get(0);
        listMock.size();
    }

    @Test
    public void testMockListGet_UsingBDD() {
        //Given
        List<String> listMock = mock(List.class);
        given(listMock.get(anyInt())).willReturn("SiliconianScholar");

        //When
        String firstElement = listMock.get(0);
        //Then
        assertThat(firstElement, is("SiliconianScholar"));
    }
}