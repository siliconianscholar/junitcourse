package com.in28minutes.data.api;

import java.util.List;

// Create TodoServiceStub
// Test TodoBusinessImpl using TodoServiceStub
public interface TodoService {
	List<String> retrieveTodos(String user);
	void deleteTodo(String todo);
}