package com.in28minutes.junit.helper;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class ArraysCompareTest {

    @Test
    public void testSortArray_RandomNumbers() {
        int[] numbers = {12, 3, 4, 1};
        int[] expected = {1, 3, 4, 12};
        Arrays.sort(numbers);
        //assertEquals(expected, numbers); checks if they are the same object, its a different object in memory but has the same values
        assertArrayEquals(expected, numbers);
    }

    @Test(expected = NullPointerException.class)
    public void testSortArray_NullArray() {
        int[] numbers = null;
        Arrays.sort(numbers);
    }

    @Test(timeout = 100)
    public void testSort_Performance() {
        int[] array = {12, 23, 4};
        for (int i = 0; i <= 1000000; i++) { // Takes 14ms to sort
            array[0] = i;
            Arrays.sort(array);
        }
    }
}