# JUnitCourse

## Description
Welcome to the JUnit Course repository! This repository contains step-by-step commits and examples designed to help you learn and master JUnit, the popular Java testing framework.

## About the Course
This course is designed to provide you with a comprehensive understanding of JUnit and how to effectively write unit tests for your Java projects. Whether you're a beginner looking to understand the basics or an experienced developer aiming to enhance your testing skills, this course has something for everyone.

## What's Included
- **Step-by-Step Commits:** Each commit in this repository represents a specific lesson or concept covered in the course. By reviewing the commit history, you can easily track your progress and revisit specific topics as needed.
- **Example Code:** Alongside the commits, you'll find example code demonstrating various testing scenarios and best practices. These examples are designed to illustrate key concepts and help reinforce your understanding of JUnit.

## Getting Started
To get started with the course, simply clone this repository to your local machine and follow along with the commits. Each commit message contains a brief description of the changes made and the corresponding lesson or concept it covers. Feel free to explore the example code and experiment with writing your own tests!

## Course Outline
1. Setting up 1st JUnit
2. 1st successful assertEquals
3. Refatoring JUnit tests
4. All conditions covered for `truncateAInFirst2Positions` method
5. assertFalse & assertTrue test cases for `areFirstAndLastTwoCharactersTheSame` method
6. @Before, @After, @BeforeClass, @AfterClass Usage
7. assertArrayEquals usage

## Installation
1. Checkout the code from [GitLab](https://gitlab.com/siliconianscholar/junitcourse)
2. Install Intellij IDE on your system [For Mac users](https://www.jetbrains.com/idea/download/?section=mac)
3. Open the project in IDE
4. Run tests or write tests to play with the codebase

## Support
[Connect with me on LinkedIn](https://www.linkedin.com/in/aindrilapolley/)

#### TODO
- [ ] Refactor the whole code
- [ ] Create a document for contribution
- [ ] Laydown contribution guidelines
- [ ] Attach the link for contribution here
- [ ] Add document for challenges faced while creating the project
- [ ] Add solution to those challenges in the same document
- [ ] Provide the link here

## Feedback and Contributions
Your feedback is valuable! If you have any questions, suggestions, or encounter any issues while working through the course material, please don't hesitate to open an issue or reach out to the course instructor via [LinkedIn](https://www.linkedin.com/in/aindrilapolley/). Additionally, contributions in the form of bug fixes, improvements to example code, or additional exercises are always welcome!

Happy learning!