package com.in28minutes.business;

import com.in28minutes.data.api.TodoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class TodoBusinessImplMockitoInjectMocksTest {

    /**
     * So basically the way you can look at rules is that like
     * with runners, you can only have one runner. So it kinds of makes the test very brittle
     * or very dependent on a specific runner. And whenever you want to do something else then you are really tied-up
     * because you cannot use another runner. So instead of using a runner, you can use multiple rules
     * and these rules can do this setup for you. So JUnit is now moving away from runners and moving more towards rules
     * because rules makes the test more flexible. You can have multiple rules defined and each one independent of each other.
     */
    /*@Rule
      public MockitoRule mockitoRule = MockitoJUnit.rule();
     */
    @Mock
    TodoService todoServiceMock;

    @InjectMocks
    TodoBusinessImpl todoBusinessImpl;

    @Captor
    ArgumentCaptor<String> stringArgumentCaptor;

    @Test
    public void testRetrieveTodosRelatedToSpring_usingAMock() {
        List<String> todos = Arrays.asList("Learn Spring MVC", "Learn Spring",
                "Learn to Dance");
        when(todoServiceMock.retrieveTodos("Aindrila")).thenReturn(todos);

        List<String> filteredtodos = todoBusinessImpl
                .retrieveTodosRelatedToSpring("Aindrila");

        assertEquals(2, filteredtodos.size());
    }

    @Test
    public void testRetrieveTodosRelatedToSpring_withEmptyList() {
        List<String> todos = Arrays.asList();
        when(todoServiceMock.retrieveTodos("Aindrila")).thenReturn(todos);

        List<String> filteredtodos = todoBusinessImpl
                .retrieveTodosRelatedToSpring("Aindrila");

        assertEquals(0, filteredtodos.size());
    }

    @Test
    public void usingMockito_UsingBDD() {
        //given
        List<String> allTodos = Arrays.asList("Learn Spring MVC",
                "Learn Spring", "Learn to Dance");
        given(todoServiceMock.retrieveTodos("SiliconianScholar")).willReturn(allTodos);

        //when
        List<String> todos = todoBusinessImpl
                .retrieveTodosRelatedToSpring("SiliconianScholar");

        //then
        assertThat(todos.size(), is(2));
    }

    @Test
    public void testDeleteTodosNotRelatedToSpring_UsingBDD() {
        //Given
        List<String> todos = Arrays.asList("Learn Spring MVC",
                "Learn Spring", "Learn to Dance");
        given(todoServiceMock.retrieveTodos("SiliconianScholar")).willReturn(todos);

        //When
        todoBusinessImpl.deleteTodosNotRelatedToSpring("SiliconianScholar");

        //Then
        verify(todoServiceMock).deleteTodo("Learn to Dance");
        //verify if called never
        verify(todoServiceMock, never()).deleteTodo("Learn Spring MVC");
        verify(todoServiceMock, never()).deleteTodo("Learn Spring");
        //called atLeastOnce, atLeast
        verify(todoServiceMock, times(1)).deleteTodo("Learn to Dance");

        //Alternative to verify for "Then"
        then(todoServiceMock).should().deleteTodo("Learn to Dance");
        then(todoServiceMock).should(never()).deleteTodo("Learn Spring MVC");
        then(todoServiceMock).should(never()).deleteTodo("Learn Spring");
    }

    //Declare Argument Captor
    //Define Argument Captor on specific method call
    //Capture the argument
    @Test
    public void testDeleteTodosNotRelatedToSpring_UsingBDD_argumentCapture() {
        //Given
        List<String> todos = Arrays.asList("Learn Spring MVC",
                "Learn Spring", "Learn to Dance");
        given(todoServiceMock.retrieveTodos("SiliconianScholar")).willReturn(todos);

        //When
        todoBusinessImpl.deleteTodosNotRelatedToSpring("SiliconianScholar");

        //Then
        then(todoServiceMock).should().deleteTodo(stringArgumentCaptor.capture());
        assertThat(stringArgumentCaptor.getValue(), is("Learn to Dance"));
    }

    @Test
    public void testDeleteTodosNotRelatedToSpring_UsingBDD_argumentCaptureMultipleTimes() {
        //Given
        List<String> todos = Arrays.asList("Learn To Rock and Roll",
                "Learn Spring", "Learn to Dance");
        given(todoServiceMock.retrieveTodos("SiliconianScholar")).willReturn(todos);

        //When
        todoBusinessImpl.deleteTodosNotRelatedToSpring("SiliconianScholar");

        //Then
        then(todoServiceMock).should(times(2)).deleteTodo(stringArgumentCaptor.capture());
        assertThat(stringArgumentCaptor.getAllValues().size(), is(2));
    }
}